import logo from "./logo.svg";
import "./App.css";
import { BrowserRouter, Route, Routes } from "react-router-dom";
import HomePage from "./Pages/HomePage/HomePage";
import LoginPage from "./Pages/LoginPage/LoginPage";
import NotFoundPage from "./Pages/NotFoundPage/NotFoundPage";
import DetailPage from "./Pages/DetailPage/DetailPage";
import LayOut from "./HigherOrderComponents/LayOut";
import AdminUserPage from "./Pages/AdminUserPage/AdminUserPage";
import AdminMoviePage from "./Pages/AdiminMoviePage/AdminMoviePage";
import Spinner from "./Spinner/Spinner";
import SingupPage from "./Pages/SinginPage/SingupPage";

function App() {
  return (
    <div>
      {/* <Spinner /> */}
      <BrowserRouter>
        <Routes>
          <Route
            path="/"
            element={
              <LayOut>
                <HomePage />
              </LayOut>
            }
          />
          <Route path="/login" element={<LoginPage />} />
          <Route path="/singup" element={<SingupPage />} />
          <Route
            path="/detail/:id"
            element={
              <LayOut>
                <DetailPage />
              </LayOut>
            }
          />
          <Route path="/admin/user" element={<AdminUserPage />} />
          <Route path="/admin/movie" element={<AdminMoviePage />} />
          <Route path="*" element={<NotFoundPage />} />
        </Routes>
      </BrowserRouter>
    </div>
  );
}

export default App;
