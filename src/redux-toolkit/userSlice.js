import { createSlice } from "@reduxjs/toolkit";

const initialState = {
  user: null,
};

const { actions, reducer } = createSlice({
  name: "userSlice",
  initialState,
  reducers: {
    setUserInfo: (state, action) => {
      state.user = action.payload;
    },
  },
});

export const { setUserInfo } = actions;

export default reducer;
