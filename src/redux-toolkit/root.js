import { combineReducers } from "@reduxjs/toolkit";
import userSlice from "./userSlice";
import spinnerSlicer from "./SpinnerSlice";
// import userSingup from "./userSingup";

const rootReducer = combineReducers({ userSlice, spinnerSlicer });
export default rootReducer;
