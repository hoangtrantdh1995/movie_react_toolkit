import { createSlice } from "@reduxjs/toolkit";

const initialState = {
  isLoading: true,
};

const { actions, reducer } = createSlice({
  name: "spinnerSlice",
  initialState,
  reducers: {
    setLoadingOn: (state, payload) => {
      state.isLoading = true;
    },
    setLoadingOff: (state, payload) => {
      state.isLoading = false;
    },
  },
});

export const { setLoadingOn, setLoadingOff } = actions;

export default reducer;

// rxslice
