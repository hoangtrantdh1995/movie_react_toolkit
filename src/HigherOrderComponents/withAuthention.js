// import { Component } from "react";
// import { userLocalService } from "../Pages/service/localService";

import { useEffect } from "react";
import { useSelector } from "react-redux";
import { userLocalService } from "../Pages/service/localService";
import userSlice from "../redux-toolkit/userSlice";

// const withAuth = (WrappedComponent) => {
//   class withAuth extends Component {
//     componentDidMount() {
//       if (userLocalService.get()?.maLoaiNguoiDung !== "QuanTri")
//         window.location.href = "/";
//     }
//     render() {
//       return <WrappedComponent />;
//     }
//   }
//   return withAuth;
// };

// export default withAuth;

const withAuth = (WrappedComponent) => {
  function WithAuth() {
    let user = useSelector((state) => state.userSlice.user);
    useEffect(() => {
      if (user?.maLoaiNguoiDung !== "QuanTri") window.location.href = "/";
    }, []);
    return <WrappedComponent />;
  }
  return WithAuth;
};

export default withAuth;
