import React from "react";
import bg_anime from "../asset/87956-christmas-snowball.json";
import Lottie from "lottie-react";

export default function LayOutLogin({ children }) {
  return (
    <div className="h-screen flex justify-center items-center bg-red-300">
      <div className="container p-5 rounded bg-white flex">
        <div className="w-1/2">
          <Lottie animationData={bg_anime} loop={true} />
        </div>
        <div className="w-1/2 pt-28">{children}</div>
      </div>
    </div>
  );
}
