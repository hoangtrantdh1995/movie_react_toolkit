import React from "react";
import Footter from "../Pages/Footer/Footter";
import Header from "../Pages/Header/Header";

export default function LayOut({ children }) {
  return (
    <div>
      <Header />
      {children}
      <Footter />
    </div>
  );
}
