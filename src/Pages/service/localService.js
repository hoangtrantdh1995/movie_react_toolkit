// export const USER_LOCAL = "USER_LOCAL";

// export const userLocalService = {
//   // lấy dữ liệu tư local
//   get: () => {
//     let userJson = localStorage.getItem(USER_LOCAL);
//     // cách 1:
//     // if (userJson) {
//     //     return JSON.parse(userJson);
//     // } else {
//     //     return null;
//     // }

//     //  cách 2:
//     return userJson ? JSON.parse(userJson) : null;
//   },
//   // lưu dữ liệu xuống local
//   set: (userData) => {
//     let userJson = JSON.stringify(userData);
//     localStorage.setItem(USER_LOCAL, userJson);
//   },

//   // xóa dữ liệu local
//   remove: () => {
//     localStorage.removeItem(USER_LOCAL);
//   },
// };

// // npm i -g typescript

// // tsc --version
