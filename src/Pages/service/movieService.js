import { https } from "./configURL"


export const movieService = {
    getMovieList: () => {
        return https.get(`/api/QuanLyPhim/LayDanhSachPhim?maNhom=GP05`);
    },
    getMovieByTheater: () => {
        return https.get(`/api/QuanLyRap/LayThongTinLichChieuHeThongRap`);
    },
};