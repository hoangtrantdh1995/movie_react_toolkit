import { https } from "./configURL";

export const getUserList = () => {
  return https.get(`/api/QuanLyNguoiDung/LayDanhSachNguoiDung?MaNhom=GP00`);
};

export const deleteUser = (account) => {
  return https.delete(
    `https://movienew.cybersoft.edu.vn/api/QuanLyNguoiDung/XoaNguoiDung?TaiKhoan=${account}`
  );
};

export const putUser = (account) => {
  return https.put(`/api/QuanLyNguoiDung/CapNhatThongTinNguoiDung`);
};
