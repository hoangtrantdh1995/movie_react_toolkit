import { https } from "./configURL";

export const postLogin = (data) => {
  return https.post("/api/QuanLyNguoiDung/DangNhap", data);
};

export const postSingup = (data) => {
  return https.post(`/api/QuanLyNguoiDung/DangKy`, data);
};

export const getMovieInfo = (data) => {
  return https.get(`/api/QuanLyPhim/LayThongTinPhim?MaPhim=${data}`);
};
