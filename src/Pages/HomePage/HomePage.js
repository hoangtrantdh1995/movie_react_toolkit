import React, { useEffect, useState } from 'react'
import { movieService } from '../service/movieService';
import MovieList from './MovieList';
import MovieTab from './MovieTab';

export default function HomePage() {
    const [movieArr, setMovieArr] = useState([]);

    useEffect(() => {
        movieService.getMovieList()
            .then((res) => {
                console.log(res);
                setMovieArr(res.data.content);
            })
            .catch((err) => {
                console.log(err);
            });
    }, [])

    return (
        <div>
            <div className="container mx-auto">
                <MovieList movieArr={movieArr} />
                <MovieTab />
            </div>
        </div>
    )
}
