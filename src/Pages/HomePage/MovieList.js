import React from "react";
import { Card } from "antd";
import { NavLink } from "react-router-dom";

const { Meta } = Card;

export default function MovieList({ movieArr }) {
    const renderMovieList = () =>
        movieArr.slice(0, 15).map((item, index) => {
            return (
                <Card
                    key={index}
                    hoverable
                    style={{
                        width: 240,
                    }}
                    cover={<img className="h-80" alt="example" src={item.hinhAnh} />}
                >
                    <Meta
                        title={<h2 className="text-blue-500 h10">{item.tenPhim}</h2>}
                        description={
                            <h4 className="h-20">
                                {item.moTa.length < 50
                                    ? item.moTa
                                    : item.moTa.slice(0, 50) + "..."}
                            </h4>
                        }
                    />
                    <NavLink
                        className="bg-red-500 px-5 py-2 text-white rounded"
                        to={`/detail/${item.maPhim}`}
                    >
                        Xem chi tiết
                    </NavLink>
                </Card>
            );
        });

    return (
        <div className="grid grid-cols-2 md:grid-cols-3 lg:grid-cols-5 gap-5">
            {renderMovieList()}
        </div>
    );
}
