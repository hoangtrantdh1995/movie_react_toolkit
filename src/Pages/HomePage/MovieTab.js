import React, { useEffect, useState } from "react";
import { movieService } from "../service/movieService";
import { Tabs } from "antd";
import MovieItemTab from "./MovieItemTab";
const onChange = (key) => {
  console.log(key);
};

export default function MovieTab() {
  // useEffectSnippet
  const [dataMovie, setDataMovie] = useState([]);

  useEffect(() => {
    movieService
      .getMovieByTheater()
      .then((res) => {
        console.log(res, "tab");
        setDataMovie(res.data.content);
      })
      .catch((err) => {
        console.log(err);
      });
  }, []);

  const renderDanhSachPhimTheoCumRap = (cumRap) => {
    return (
      <div>
        {cumRap.danhSachPhim.map((movie, index) => {
          return (
            <div>
              <MovieItemTab movie={movie} key={index} />
            </div>
          );
        })}
      </div>
    );
  };

  const renderCumRapTheoHeThong = (heThongRap) => {
    return heThongRap.lstCumRap.map((cumRap, index) => {
      return {
        label: (
          <div className="w-44" key={index}>
            <h4>{cumRap.tenCumRap} </h4>
            <p className="truncate">{cumRap.diaChi}</p>
          </div>
        ),
        key: cumRap.maCumRap,
        children: (
          <div style={{ height: 400, overflowY: "scroll" }}>
            {renderDanhSachPhimTheoCumRap(cumRap)}
          </div>
        ),
      };
    });
  };

  const renderHeThongRap = () => {
    return dataMovie.map((heThongRap, index) => {
      console.log("cumrap", heThongRap);
      return {
        label: <img className="w-20 h-20" src={heThongRap.logo} alt="" />,
        key: heThongRap.maHeThongRap,
        children: (
          <Tabs
            style={{
              height: 400,
            }}
            tabPosition="left"
            defaultActiveKey="1"
            onChange={onChange}
            items={renderCumRapTheoHeThong(heThongRap)}
          />
        ),
      };
    });
  };

  return (
    <div className="container mx-auto mt-20">
      <Tabs
        style={{
          height: 400,
        }}
        tabPosition="left"
        defaultActiveKey="1"
        onChange={onChange}
        items={renderHeThongRap()}
      />
    </div>
  );
}
