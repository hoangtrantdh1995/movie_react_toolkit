import React from "react";
import { Desktop, Tablet, Mobile } from "../../HigherOrderComponents/Reponsive";
import MovieTab from "../HomePage/MovieTab";
import FooterDesktop from "./FooterDesktop";
import FooterMobile from "./FooterMobile";
import FooterTablet from "./FooterTablet";

export default function Footter() {
  return (
    <div>
      <Desktop>
        <FooterDesktop />
      </Desktop>

      <Tablet>
        <FooterTablet />
      </Tablet>

      <Mobile>
        <FooterMobile />
      </Mobile>
    </div>
  );
}
