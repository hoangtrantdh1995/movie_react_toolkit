import React from "react";
import { useDispatch, useSelector } from "react-redux";
import { NavLink } from "react-router-dom";
import { setUserInfo, setUserLogout } from "../../redux-toolkit/userSlice";
import { userLocalService } from "../service/localService";

export default function UserNav() {
  // lấy dữ liệu từ store
  let user = useSelector((state) => state.userSlice.user);
  console.log("use", user);
  const dispatch = useDispatch();

  const handleLogOut = () => {
    // xóa khỏi local
    // userLocalService.remove();
    // thoát ra trang đang đứng
    // window.location.reload();
    // thoát sang trang khác
    // window.location.href = "/login";
    dispatch(setUserInfo(null));
  };

  const renderContent = () => {
    if (user) {
      // đã đăng nhập
      return (
        <>
          <span>{user?.hoTen}</span>
          <button
            className="border-2 border-black px-5 py2 rounded"
            onClick={handleLogOut}
          >
            Đăng xuất
          </button>
        </>
      );
    } else {
      return (
        // để thẻ rỏng thay cho thẻ div để return dc nhiều giá trị, nhưng ko bị trùng với thẻ div khi render
        <>
          <NavLink to={`/login`}>
            <button className="border-2 border-black px-5 py5 rounded">
              Đăng nhập
            </button>
          </NavLink>
          <NavLink to={`/singup`}>
            <button className="border-2 border-black px-5 py5 rounded">
              Đăng ký
            </button>
          </NavLink>
        </>
      );
    }
  };

  return <div className="space-x-3">{renderContent()}</div>;
}
