import React, { useEffect, useState } from "react";
import { deleteUser, getUserList } from "../service/adminService";
import { Button, message, Space, Table, Tag } from "antd";
import { userColums } from "./utils";
import withAuth from "../../HigherOrderComponents/withAuthention";

const columns = [
  {
    title: "Name",
    dataIndex: "name",
    key: "name",
    render: (text) => <a>{text}</a>,
  },
  {
    title: "Age",
    dataIndex: "age",
    key: "age",
  },
  {
    title: "Address",
    dataIndex: "address",
    key: "address",
  },
  {
    title: "Tags",
    key: "tags",
    dataIndex: "tags",
    render: (_, { tags }) => (
      <>
        {tags.map((tag) => {
          let color = tag.length > 5 ? "geekblue" : "green";
          if (tag === "loser") {
            color = "volcano";
          }
          return (
            <Tag color={color} key={tag}>
              {tag.toUpperCase()}
            </Tag>
          );
        })}
      </>
    ),
  },
  {
    title: "Action",
    key: "action",
    render: (_, record) => (
      <Space size="middle">
        <a>Invite {record.name}</a>
        <a>Delete</a>
      </Space>
    ),
  },
];
const data = [
  {
    key: "1",
    name: "John Brown",
    age: 32,
    address: "New York No. 1 Lake Park",
    tags: ["nice", "developer"],
  },
  {
    key: "2",
    name: "Jim Green",
    age: 42,
    address: "London No. 1 Lake Park",
    tags: ["loser"],
  },
  {
    key: "3",
    name: "Joe Black",
    age: 32,
    address: "Sidney No. 1 Lake Park",
    tags: ["cool", "teacher"],
  },
];
function AdminUserPage() {
  const [userArr, setUserArr] = useState([]);

  useEffect(() => {
    let handleRemoveUser = (account) => {
      deleteUser(account)
        .then((res) => {
          console.log(res);
          message.success("Xóa thành công");
          // render lại sau khi xóa thành công
          fetchUserList();
        })
        .catch((err) => {
          console.log(err);
          // message.error("Xóa thất bại");
          message.error(err.response.data.content);
        });
    };

    let fetchUserList = () => {
      getUserList()
        .then((res) => {
          console.log(res);
          // setUserArr(res.data.content);
          let userList = res.data.content.map((item) => {
            return {
              ...item,
              key: item.taiKhoan,
              action: (
                <>
                  <div className="space-x-5">
                    <Button
                      type="primary"
                      danger
                      onClick={() => {
                        handleRemoveUser(item.taiKhoan);
                      }}
                    >
                      Xóa
                    </Button>
                    <Button className="bg-blue-600 text-white">Sửa</Button>
                  </div>
                </>
              ),
            };
          });
          setUserArr(userList);
        })
        .catch((err) => {
          console.log(err);
        });
    };

    fetchUserList();
  }, []);

  return (
    <div>
      <Table columns={userColums} dataSource={userArr} />
    </div>
  );
}

export default withAuth(AdminUserPage);
