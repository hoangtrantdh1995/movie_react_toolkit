import { Tag } from "antd";

export const userColums = [
  {
    title: "Họ tên",
    dataIndex: "hoTen",
    key: "hoTen",
  },
  {
    title: "Tài Khoản",
    dataIndex: "taiKhoan",
    key: "taiKhoan",
  },
  {
    title: "Email",
    dataIndex: "email",
    key: "email",
  },
  {
    title: "Loại người dùng",
    dataIndex: "maLoaiNguoiDung",
    key: "maLoaiNguoiDung",
    render: (text) => {
      if (text == "QuanTri") {
        return <Tag color="red">Admin</Tag>;
      } else {
        return <Tag color="blue">User</Tag>;
      }
    },
  },
  {
    title: "Hành động",
    dataIndex: "action",
    key: "action",
  },
];
