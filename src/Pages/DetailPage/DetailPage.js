import React, { useEffect, useState } from "react";
import { useParams } from "react-router-dom";
import { getMovieInfo } from "../service/userService";
import { Button, Modal, Image } from "antd";
import { useSelector } from "react-redux";

export default function DetailPage() {
  let params = useParams();
  const [movieInfo, setMovieInfo] = useState();
  useEffect(() => {
    getMovieInfo(params.id)
      .then((res) => {
        console.log("1234567", res);
        setMovieInfo(res.data.content);
        console.log("res.data.content: ", res.data.content);
      })
      .catch((err) => {
        console.log(err);
      });
  }, [params.id]);

  const [isModalOpen, setIsModalOpen] = useState(false);
  const showModal = (e) => {
    console.log("e: ", e);
    setIsModalOpen(true);
  };
  const handleOk = () => {
    setIsModalOpen(false);
  };
  const handleCancel = (e) => {
    setIsModalOpen(false);
  };

  const renderMovieInfo = () => {
    return (
      <>
        <img src={movieInfo?.hinhAnh} onClick={showModal} alt="" />
        {/* <Image width={200} src={movieInfo?.trailer} onClick={showModal} /> */}
        {/* <Button className="bg-red-500" onClick={showModal}>
          Open Modal
        </Button> */}
        <Modal
          title={`Trailer - ${movieInfo.tenPhim}`}
          open={isModalOpen}
          onOk={handleOk}
          onCancel={handleCancel}
          destroyOnClose={true}
          okType={"default"}
          // style={{ background: "black" }}
        >
          <div
            className="embed-responsive embed-responsive-21by9 relative w-full overflow-hidden"
            style={{ paddingTop: "42.857143%" }}
          >
            <iframe
              className="embed-responsive-item absolute top-0 right-0 bottom-0 left-0 w-full h-full"
              src={movieInfo?.trailer}
              allowFullScreen
              data-gtm-yt-inspected-2340190_699="true"
              id={240632615}
            />
          </div>
        </Modal>
      </>
      /* // <div>
      //   <img src={movieInfo?.hinhAnh} alt="" />
      //   <div */
      //     class="embed-responsive embed-responsive-21by9 relative w-full overflow-hidden"
      //     style="padding-top: 42.857143%"
      //   >
      //     <iframe
      //       class="embed-responsive-item absolute top-0 right-0 bottom-0 left-0 w-full h-full"
      //       src={movieInfo?.trailer}
      //       allowfullscreen=""
      //       data-gtm-yt-inspected-2340190_699="true"
      //       id="240632615"
      //     ></iframe>
      //   </div>
      // </div>
    );
  };

  return (
    <div className="text-center">
      {/* <h2 className=" text-yellow-500  font-black">Mã phim : {params.id}</h2> */}
      {renderMovieInfo()}
    </div>
  );
}
