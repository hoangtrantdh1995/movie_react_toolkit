import React from "react";
import { Button, Checkbox, Form, Input, message } from "antd";
import { postLogin } from "../service/userService";
import { useNavigate } from "react-router-dom";
import { useDispatch } from "react-redux";
import { setUserInfo } from "../../redux-toolkit/userSlice";
import LayOutLogin from "../../HigherOrderComponents/LayOutLogin";

export default function LoginPage() {
  let navigate = useNavigate();
  // update dữ liệu lên store
  let dispatch = useDispatch();

  // const onFinishReduxThuk = (values) => {
  //   const handleNavigate = () => {
  //     setTimeout(() => {
  //       // window.location.href -> load lại trang và đi tới trang khác
  //       // window.location.href = "/";
  //       navigate("/");
  //       // navigate("/") -> chuyển tới trang home page
  //     }, 1000);
  //   };

  //   dispatch(setUserActionService(values, handleNavigate));
  // };

  const onFinish = (values) => {
    postLogin(values)
      .then((res) => {
        console.log(res);
        message.success("Đăng nhập thành công");
        // dispatch(setUserAction(res.data.content));
        dispatch(setUserInfo(res.data.content));
        // sau khi đăng nhập thành công sẽ đẩy ra trang chủ
        setTimeout(() => {
          // window.location.href -> load lại trang và đi tới trang khác
          // window.location.href = "/";
          navigate("/");
          // navigate("/") -> chuyển tới trang home page
        }, 1000);
      })
      .catch((err) => {
        console.log(err);
        message.error("Đăng nhập thật bại");
      });
  };

  const onFinishFailed = (errorInfo) => {
    console.log("Failed:", errorInfo);
  };
  return (
    <LayOutLogin>
      <Form
        layout="vertical"
        name="basic"
        labelCol={{ span: 8 }}
        wrapperCol={{ span: 24 }}
        initialValues={{ remember: true }}
        onFinish={onFinish}
        onFinishFailed={onFinishFailed}
        autoComplete="off"
      >
        <Form.Item
          label="Username"
          name="taiKhoan"
          rules={[{ required: true, message: "Please input your username!" }]}
        >
          <Input />
        </Form.Item>

        <Form.Item
          label="Password"
          name="matKhau"
          rules={[{ required: true, message: "Please input your password!" }]}
        >
          <Input.Password />
        </Form.Item>

        <Form.Item wrapperCol={{ span: 16 }}>
          <Button className="bg-blue-500" htmlType="submit">
            Submit
          </Button>
        </Form.Item>
      </Form>
    </LayOutLogin>
  );
}

// admin005  admin0031
