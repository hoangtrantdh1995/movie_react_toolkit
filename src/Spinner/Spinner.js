import React from "react";
import { useSelector } from "react-redux";
import { RingLoader } from "react-spinners";

export default function Spinner() {
  let isLoading = useSelector((state) => {
    return state.SpinnerSlice.isLoading;
  });
  return isLoading ? (
    <div className="fixed bg-black w-screen h-screen top-0 l-0 z-50 flex items-center justify-center">
      <RingLoader size="150" speedMultiplier={1} color="#36d7b7" />
    </div>
  ) : (
    <></>
  );
}
